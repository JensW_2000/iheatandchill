/*
Copyright 2018 Jens Warkentin (jw@innovasoft.de)

Licensed under the Non-Profit Open Software License 3.0 (NPOSL-3.0) (the "License").
You may not use this file except in compliance with the License.

You may obtain a copy of the License at
https://opensource.org/licenses/NPOSL-3.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include "Trend.h"
#include <math.h>
#include "Globals.h"

Trend::Trend() {
}

void Trend::begin()
{
	reset();
}

float Trend::getAvg(uint8_t limit)
{
	float sum = 0;

	if (limit > _arraySize - 1)
		limit = _arraySize - 1;
	for (uint8_t i = 0; i <= limit; i++)
	{
		sum += _valueStore[i];
		yield();
	}
	return sum / limit + 1;
}


void Trend::reset()
{
	for (uint8_t i = 0; i < _arraySize; i++)
	{
		_valueStore[i] = 0;
		yield();
	}
}

void Trend::setPoint(float dataPointValue)
{
	if (dataPointValue == 0)
		return;
	for (uint8_t i = 0; i < _arraySize; i++)
	{
		if (_valueStore[i] == 0)
		{
			_valueStore[i] = dataPointValue;
			return;
		}
		yield();
	}

	// array voll
	for (uint8_t i = 0; i < _arraySize; i++)
	{
		_valueStore[i - 1] = _valueStore[i - 1];
		yield();
	}
	_valueStore[_arraySize - 1] = dataPointValue;
}

TrendResult Trend::value(uint8_t minValues)
{
	uint cnt = 0;
	for (uint i = 0; i < _arraySize; i++)
	{
		if (_valueStore[i] != 0)
			cnt += 1;
		yield();
	}

	if (cnt < minValues || cnt == 0)
		return TrendResult::trUndefined;

	float* xVals = new float[cnt];
	float* yVals = new float[cnt];

	for (uint i = 0; i < _arraySize; i++)
	{
		if (_valueStore[i] != 0)
		{
			xVals[i] = i;
			yVals[i] = _valueStore[i];
		}
		yield();
	}

	float rsquared = 0;
	float yintercept = 0;
	float slope = 0;

	linearRegression(xVals, yVals, 0, cnt, rsquared, yintercept, slope);

	if (slope > 0)
		return TrendResult::trRisingEdge;
	if (slope < 0)
		return TrendResult::trFallingEdge;
	return TrendResult::trUndefined;
};

/*
// https://gist.github.com/tansey/1375526#file-gistfile1-cs-L1

/// <summary>
/// Fits a line to a collection of (x,y) points.
/// </summary>
/// <param name="xVals">The x-axis values.</param>
/// <param name="yVals">The y-axis values.</param>
/// <param name="inclusiveStart">The inclusive inclusiveStart index.</param>
/// <param name="exclusiveEnd">The exclusive exclusiveEnd index.</param>
/// <param name="rsquared">The r^2 value of the line.</param>
/// <param name="yintercept">The y-intercept value of the line (i.e. y = ax + b, yintercept is b).</param>
/// <param name="slope">The slop of the line (i.e. y = ax + b, slope is a).</param>
*/
void Trend::linearRegression(float* xVals, float* yVals,
	int inclusiveStart, int exclusiveEnd,
	float& rsquared, float& yintercept, float& slope)

{
	float sumOfX = 0;
	float sumOfY = 0;
	float sumOfXSq = 0;
	float sumOfYSq = 0;
	float ssX = 0;
//	float ssY = 0;
	float sumCodeviates = 0;
	float sCo = 0;
	float count = exclusiveEnd - inclusiveStart;

	for (int ctr = inclusiveStart; ctr < exclusiveEnd; ctr++)
	{
		float x = xVals[ctr];
		float y = yVals[ctr];
		sumCodeviates += x * y;
		sumOfX += x;
		sumOfY += y;
		sumOfXSq += x * x;
		sumOfYSq += y * y;
	}
	ssX = sumOfXSq - ((sumOfX * sumOfX) / count);
//	ssY = sumOfYSq - ((sumOfY * sumOfY) / count);
	float RNumerator = (count * sumCodeviates) - (sumOfX * sumOfY);
	float RDenom = (count * sumOfXSq - (sumOfX * sumOfX))
		* (count * sumOfYSq - (sumOfY * sumOfY));
	sCo = sumCodeviates - ((sumOfX * sumOfY) / count);

	float meanX = sumOfX / count;
	float meanY = sumOfY / count;
	float dblR = RNumerator / sqrt(RDenom);
	rsquared = dblR * dblR;
	yintercept = meanY - ((sCo / ssX) * meanX);
	slope = sCo / ssX;
}

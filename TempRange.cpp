/*
Copyright 2018 Jens Warkentin (jw@innovasoft.de)

Licensed under the Non-Profit Open Software License 3.0 (NPOSL-3.0) (the "License").
You may not use this file except in compliance with the License.

You may obtain a copy of the License at
https://opensource.org/licenses/NPOSL-3.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

---------------------------------------------------------------------------------------------------------
Special thanks to Samuel Lang.
Some lines of code are borrowed from his iSpindel project (https://github.com/universam1/iSpindel).
---------------------------------------------------------------------------------------------------------

Code hints:
There are no source code comments in this project. I used self-speaking names for methods, constants and variables.
See the C code as self-explaining text and a large comment in itself.
*/
#include "Globals.h"
#include "TempRange.h"

TempRange::TempRange(float defaultIdleTime, float defaultWorkingTime)
{
	this->WORKING_TIME = defaultWorkingTime;
	this->IDLE_TIME = defaultIdleTime;
}

void TempRange::set(float targetTemp, float hysterese, float maxUndershot, float maxOvershot)
{
	this->_targetTemp = targetTemp;
	this->_hysterese = hysterese;
	this->_rangeLowerTemp = targetTemp - hysterese;
	this->_rangeUpperTemp = targetTemp + hysterese;

	if (maxOvershot > maxUndershot)
	{
		this->_maxOvershot = maxOvershot;
		this->_maxUndershot = maxUndershot;
	}
	else
	{
		this->_maxOvershot = maxUndershot;
		this->_maxUndershot = maxOvershot;
	}
}

void TempRange::updateCurrentTemp(float temp)
{
	_currentTemp = temp;
}

void TempRange::updateAmbientImpact(float temp)
{
	_ambientImpact = temp;
}

bool TempRange::isInBounds(AmbientImpactRelatedTo ambientImpactRelatedTo)
{
	switch (ambientImpactRelatedTo)
	{
	case airWillChill:
		return _currentTemp + _ambientImpact >= _rangeLowerTemp  && _currentTemp <= _rangeUpperTemp;
		break;
	case airWillHeat:
		return _currentTemp >= _rangeLowerTemp  && _currentTemp + _ambientImpact <= _rangeUpperTemp;
		break;
	default:
		return _currentTemp >= _rangeLowerTemp  && _currentTemp <= _rangeUpperTemp;
		break;
	}
}

bool TempRange::isAbove(BoundsType boundsType, AmbientImpactRelatedTo ambientImpactRelatedTo)
{
	float ambientRelatedCurrentTemp = airNothing ? _currentTemp : _currentTemp + _ambientImpact;

	switch (boundsType)
	{
	case btTargetTemp:
		return ambientRelatedCurrentTemp > _targetTemp;
		break;
	case btRangeLowerTemp:
		return ambientRelatedCurrentTemp > _rangeLowerTemp;
		break;
	case btRangeUpperTemp:
		return ambientRelatedCurrentTemp > _rangeUpperTemp;
		break;
	case btRangeUndershotTemp:
		return _currentTemp > _targetTemp - _maxUndershot;
		break;
	case btRangeOvershotTemp:
		return _currentTemp > _targetTemp + _maxOvershot;
		break;
	default:
		return true;
		break;
	}
}

bool TempRange::isBelow(BoundsType boundsType, AmbientImpactRelatedTo ambientImpactRelatedTo)
{
	float ambientRelatedCurrentTemp = airNothing ? _currentTemp : _currentTemp + _ambientImpact;

	switch (boundsType)
	{
	case btTargetTemp:
		return ambientRelatedCurrentTemp < _targetTemp;
		break;
	case btRangeLowerTemp:
		return ambientRelatedCurrentTemp < _rangeLowerTemp;
		break;
	case btRangeUpperTemp:
		return ambientRelatedCurrentTemp < _rangeUpperTemp;
		break;
	case btRangeUndershotTemp:
		return _currentTemp < _targetTemp - _maxUndershot;
		break;
	case btRangeOvershotTemp:
		return _currentTemp < _targetTemp + _maxOvershot;
		break;
	default:
		return true;
		break;
	}
}

float TempRange::getTargetTemp()
{
	return _targetTemp;
}

float TempRange::getRangeCurrentTemp()
{
	return _currentTemp;
}

float TempRange::getRangeUpperTemp()
{
	return _rangeUpperTemp;
}

float TempRange::getRangeLowerTemp()
{
	return _rangeLowerTemp;
}

ulong TempRange::getAssumedTimeToIdle(float ambientImpact)
{
	float diffTemp;
	if (ambientImpact > 0)
	{
		diffTemp = _rangeUpperTemp - _currentTemp;
	}
	else if (ambientImpact < 0)
	{
		diffTemp = _currentTemp - _rangeLowerTemp;
	}
	else
		return IDLE_TIME;

	return 45 * fabs(diffTemp) / fabs(ambientImpact);
}

ulong TempRange::getAssumedTimeToChill(float coolingRate)
{
	float diffTemp = _currentTemp + _ambientImpact - _rangeLowerTemp;

	if (diffTemp < 0)
		return 1;
	if (coolingRate == 0)
		return WORKING_TIME;

	return  roundl(60.0 * diffTemp / fabs(coolingRate));
}

ulong TempRange::getAssumedTimeToHeat(float heatingRate)
{
	float diffTemp = _rangeUpperTemp - _currentTemp + _ambientImpact;

	if (diffTemp < 0)
		return 1;
	if (heatingRate == 0)
		return WORKING_TIME;

	return 60 * diffTemp / fabs(heatingRate);
}

float TempRange::getCurrentTempRelatedToAmbientImpact(AmbientImpactRelatedTo ambientImpactRelatedTo)
{
	return airNothing ? _currentTemp : _currentTemp + _ambientImpact;
}
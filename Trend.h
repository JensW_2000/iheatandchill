/*
Copyright 2018 Jens Warkentin (jw@innovasoft.de)

Licensed under the Non-Profit Open Software License 3.0 (NPOSL-3.0) (the "License").
You may not use this file except in compliance with the License.

You may obtain a copy of the License at
https://opensource.org/licenses/NPOSL-3.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

---------------------------------------------------------------------------------------------------------
Special thanks to Samuel Lang.
Some lines of code are borrowed from his iSpindel project (https://github.com/universam1/iSpindel).
---------------------------------------------------------------------------------------------------------

Code hints:
There are no source code comments in this project. I used self-speaking names for methods, constants and variables.
See the C code as self-explaining text and a large comment in itself.
*/
#include "Arduino.h"

enum TrendResult
{
	trUndefined,
	trFallingEdge,
	trRisingEdge
};

class Trend
{
private:
	static const uint8 _arraySize = 50;
	float _valueStore[_arraySize];
	float getAvg(uint8_t limit);
	void linearRegression(float* xVals, float* yVals,
		int inclusiveStart, int exclusiveEnd,
		float& rsquared, float& yintercept,
		float& slope); // https://gist.github.com/tansey/1375526#file-gistfile1-cs-L1
public:
	Trend();
	void begin();
	void reset();
	void setPoint(float dataPoint);
	TrendResult value(uint8_t minValues=0);

};


/*
---------------------------------------------------------------------------------------------------------
Special thanks to Samuel Lang.
This code are borrowed from his iSpindel project (https://github.com/universam1/iSpindel).
---------------------------------------------------------------------------------------------------------
*/
#define _GLOBALS_H

#pragma once
//#define USE_DMP false
#include <Arduino.h>

#ifndef DEBUG
#define DEBUG true
#endif

#ifdef NO_CONSOLE
#define CONSOLE(x) \
    do             \
    {              \
    } while (0)
#define CONSOLELN CONSOLE
#define CONSOLEF CONSOLE
#else
#define CONSOLE(x)       \
    do                   \
    {                    \
        Serial.print(x); \
    } while (0)
#define CONSOLELN(x)      \
    do                     \
    {                      \
        Serial.println(x); \
    } while (0)
#endif



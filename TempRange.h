/*
Copyright 2018 Jens Warkentin (jw@innovasoft.de)

Licensed under the Non-Profit Open Software License 3.0 (NPOSL-3.0) (the "License").
You may not use this file except in compliance with the License.

You may obtain a copy of the License at
https://opensource.org/licenses/NPOSL-3.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

---------------------------------------------------------------------------------------------------------
Special thanks to Samuel Lang.
Some lines of code are borrowed from his iSpindel project (https://github.com/universam1/iSpindel).
---------------------------------------------------------------------------------------------------------

Code hints:
There are no source code comments in this project. I used self-speaking names for methods, constants and variables.
See the C code as self-explaining text and a large comment in itself.
*/
#include "Arduino.h"

#ifndef _TEMPCORRIDOR_h
#define _TEMPCORRIDOR_h

enum BoundsType {
	btTargetTemp,
	btRangeLowerTemp,
	btRangeUpperTemp,
	btRangeUndershotTemp,
	btRangeOvershotTemp,
};

enum AmbientImpactRelatedTo {
	airNothing,
	airWillChill,
	airWillHeat,
};

class TempRange
{
private:
	float _currentTemp = 0;
	float _ambientImpact = 0;
	float _targetTemp = 16;
	float _hysterese = 0.3f;
	float _maxUndershot = 1.5f;
	float _maxOvershot = 1.5f;
	float _rangeLowerTemp = 15.7f;
	float _rangeUpperTemp = 16.3f;
	float WORKING_TIME;
	float IDLE_TIME;
public:
	TempRange(float defaultIdleTime, float defaultWorkingTime);
	void set(float targetTemp, float hysterese, float maxUndershot, float maxOvershot);
	void updateCurrentTemp (float temp);
	void updateAmbientImpact(float temp);
	bool isInBounds(AmbientImpactRelatedTo ambientImpactRelatedTo);
	bool isAbove(BoundsType boundsType, AmbientImpactRelatedTo ambientImpactRelatedTo);
	bool isBelow(BoundsType boundsType, AmbientImpactRelatedTo ambientImpactRelatedTo);
	float getTargetTemp();
	float getRangeUpperTemp();
	float getRangeLowerTemp();
	float getRangeCurrentTemp();
	ulong getAssumedTimeToIdle(float ambientImpact);
	ulong getAssumedTimeToChill(float coolingRate);
	ulong getAssumedTimeToHeat(float heatingRate);
	float getCurrentTempRelatedToAmbientImpact(AmbientImpactRelatedTo ambientImpactRelatedTo);
};

#endif


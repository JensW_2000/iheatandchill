﻿/*
Copyright 2018 Jens Warkentin (jw@innovasoft.de)

Licensed under the Non-Profit Open Software License 3.0 (NPOSL-3.0) (the "License").
You may not use this file except in compliance with the License.

You may obtain a copy of the License at
https://opensource.org/licenses/NPOSL-3.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

---------------------------------------------------------------------------------------------------------
Special thanks to Samuel Lang.
Some lines of code are borrowed from his iSpindel project (https://github.com/universam1/iSpindel). 
---------------------------------------------------------------------------------------------------------

Code hints:
There are no source code comments in this project. I used self-speaking names for methods, constants and variables.
See the C code as self-explaining text and a large comment in itself.
*/

#include "Trend.h"
#include "TempRange.h"
#include <float.h>
#include <Wire.h>
#include <OneWire.h>
#include <SPI\SPI.h>
#include <U8g2lib.h>

#include <DallasTemperature.h>
#include "Globals.h"
#include <Esp.h>
#include <FS.h>

#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <DNSServer.h>
#include <SPIFFSReadServer.h>
#include <PersWiFiManager.h>
#include <ESP8266SSDP.h>
#include <EasySSDP.h>
#include <Metro.h>
#include "Sender.h"

extern "C" {
#include "user_interface.h"
}

#define RESOLUTION 12  
#define ONE_WIRE_BUS D6
#define MODE_RELAIS D1 
#define PELTIER_RELAIS D2
#define PUMP_RELAIS D3
#define PIN_SDA D5
#define PIN_SCL D7 

#define CFGFILE "iHeatAndCool.json"

enum OperationMode
{
	idle,
	measuring,
	cooling,
	heating,
	initializing,
	dsError
};

uint8_t GradC_TileUpperLine[16] = { 6,9,9,6,0,252,254,3 ,3,3,3,3,3,3,14,12 };
uint8_t GradC_TileBottomLine[16] = { 0,0,0,0,0,15,31,48 ,48,48,48,48,48,28,12 };
uint8_t Arrow_TileUpperLine[16] = { 128,128,128,128,128,128,128,128, 144,144,160,160,192,192,128,128 };
uint8_t Arrow_TileBottomLine[16] = { 1,1,1,1,1,1,1,1, 9,9,5,5,3,3,1,1 };

uint8_t Equal_TileUpperLine[16] = { 0,0,0,32,32,32,32,32, 32,32,32,32,32,0,0,0};
uint8_t Equal_TileBottomLine[16] = { 0,0,0,1,1,1,1,1, 1,1,1,1,1,0,0,0};
uint8_t Approx_TileUpperLine[16] = { 0,64,32,144,144,144,32,64, 128,0,0,0,0,0,128,0 };
uint8_t Approx_TileBottomLine[16] = { 0,2,1,0,0,0,1,2, 4,9,18,18,18,9,4,0 };
uint8_t DummyTileLine[16]{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
uint8_t TempType_TileUpperLine[16];
uint8_t TempType_TileBottomLine[16];


uint8_t Line_Tile[128] = {
	0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8,
	0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8,
	0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8,
	0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8,
	0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8,
	0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8,
	0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8,
	0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8,
	0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8,
	0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8,
	0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8,
	0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8,
	0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8,
	0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8,
	0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8,
	0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8
};

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature DS18B20(&oneWire);
DeviceAddress dsDeviceAddress;

SPIFFSReadServer server(80);
DNSServer dnsServer;
PersWiFiManager persWM(server, dnsServer);

U8X8_SH1106_128X64_NONAME_SW_I2C oled(/* clock=*/ PIN_SCL, /* data=*/ PIN_SDA, /* reset=*/ U8X8_PIN_NONE);   // OLEDs without Reset of the Display

const uint DS_TIME = 28;
const uint IDLE_TIME = 780;
const uint MEASURING_TIME = 180;
const uint WORKING_TIME = 300;
const uint MIN_IDLE_TIME = 300;
const uint MIN_WORKING_TIME = 90;
const uint MAX_IDLE_TIME = 1800;
const uint MAX_WORKING_TIME = 1800;

bool configChanged = false;
bool configApModeChanged = false;

char* deviceName = (char*)"iHeatAndChill";
char* apModePsk = (char*)"chillmybeer!"; // default AP-mode password
char* ubiToken = (char*)"A1E-gQYaIDPRs8DYJ56ENPNtJtUq0cSiDr"; // dummy token

uint idleTime = IDLE_TIME;
uint measuringTime = MEASURING_TIME;
uint workingTime = WORKING_TIME;

float tempStartIdle;
float tempStartCooling;
float tempStartHeating;
float ambientImpact = 0;
float coolingRate = 0.075;
float heatingRate = 0.075;

float targetTemp = 16;
float currentTemp = -127;
float measuredTemp = 0;
float hysterese = 0.3;
float maxOvershot = 1.2;
float maxUndershot = 1.5;
bool debugToUbidots = false;

OperationMode lastWorkingMode = initializing;
AmbientImpactRelatedTo ambientImpactRelatedTo = airNothing;

TempRange range(IDLE_TIME, WORKING_TIME);

int ledState = HIGH;

Metro ledMetro = Metro(250);
Metro getTempMetro = Metro(5000);
Metro sendDataMetro = Metro(60000UL);
Metro idleMetro = Metro(idleTime * 1000);
Metro measuringMetro = Metro(measuringTime * 1000);
Metro workingMetro = Metro(workingTime * 1000);
Metro oneMinuteMetro = Metro(60000UL);

OperationMode operationMode = initializing;

char* lastDisplayedTempS = (char*)"0.00";
char valueString[5];

/*Header*/
bool getTemperature(bool silent = true);
void startMeasuring();
void settingsChanged();
bool saveConfig();
OperationMode getOperationMode();


/*Implementation*/
String formatBytes(size_t bytes) {
	if (bytes < 1024) {
		return String(bytes) + "B";
	}
	else if (bytes < (1024 * 1024)) {
		return String(bytes / 1024.0) + "KB";
	}
	else if (bytes < (1024 * 1024 * 1024)) {
		return String(bytes / 1024.0 / 1024.0) + "MB";
	}
	else {
		return String(bytes / 1024.0 / 1024.0 / 1024.0) + "GB";
	}
}

const char* floatToString(float aFloat, int aPrecision)
{
	dtostrf(aFloat, 2, aPrecision, valueString);
	return valueString;
}

bool stringToBool(String const& s) {
	return (s != "0") && (s != "false") && (s != "False") && (s != "FALSE");
}

void copyTileArray(uint8_t sourceArray[16], uint8_t destinationArray[16])
{
	for (int i = 0; i < 16; i++)
		destinationArray[i] = sourceArray[i];
}

const char*  operationModeToString()
{
	switch (operationMode)
	{
	case idle:
		return (char*)"Standby";
	case measuring:
		return (char*)"Messen";
	case cooling:
		return (char*)"Kühlen";
	case heating:
		return (char*)"Heizen";
	case dsError:
		return (char*)"Störung!";
	case initializing:
		return (char*)"Setup";
	default:
		return "HOOPS";
	}
}

void initDS18B20()
{

	digitalWrite(ONE_WIRE_BUS, LOW);
	delay(100);

	DS18B20.begin();
	DS18B20.setWaitForConversion(false);
	DS18B20.getAddress(dsDeviceAddress, 0);
	DS18B20.setResolution(dsDeviceAddress, RESOLUTION);
	getTemperature(false);
}

void initOled()
{
	oled.begin();
	oled.setPowerSave(0);
}

void initConfigManager()
{
	persWM.onConnect([]() {
		CONSOLE("WiFi connected: ");
		CONSOLELN(WiFi.localIP());
		EasySSDP::begin(server);
	});
	persWM.onAp([]() {
		CONSOLE("AP MODE SSID: ");
		CONSOLELN(persWM.getApSsid());
	});

	persWM.setApCredentials(deviceName, apModePsk);
	persWM.setConnectNonBlock(true);
	persWM.begin();

	SPIFFS.begin();
	{
		CONSOLELN("Flash-Memory Contents:");
		Dir dir = SPIFFS.openDir("/");
		while (dir.next()) {
			String fileName = dir.fileName();
			size_t fileSize = dir.fileSize();
			CONSOLE("FS File: ");
			CONSOLE(fileName.c_str());
			CONSOLE(" ,size: ");
			CONSOLELN(formatBytes(fileSize).c_str());
		}
	}
	

	server.on("/statusApi", []() {
		CONSOLELN("server.on /statusApi");
		StaticJsonBuffer<200> jsonBuffer;
		JsonObject &json = jsonBuffer.createObject();
		json["targetTemp"] = targetTemp;
		json["currentTemp"] = currentTemp;
		json["measuredTemp"] = measuredTemp;
		json["operationMode"] = operationModeToString();

		char jsonchar[200];
		json.printTo(jsonchar);
		server.send(200, "application/json", jsonchar);
		delay(100);

	}); //server.on statusApi

	server.on("/configApi", []() {
		CONSOLELN("server.on /configApi");

		if (server.hasArg("ubiToken")) {
			strcpy(ubiToken, server.arg("ubiToken").c_str());
			CONSOLE("SET ubiToken: ");
			CONSOLELN(ubiToken);
			configChanged = true;
		} //if
		if (server.hasArg("targetTemp")) {
			targetTemp = server.arg("targetTemp").toInt();
			CONSOLE("SET targetTemp: ");
			CONSOLELN(targetTemp);
			configChanged = true;
		} //if
		if (server.hasArg("hysterese")) {
			hysterese = server.arg("hysterese").toFloat();
			CONSOLE("SET hysterese: ");
			CONSOLELN(hysterese);
			configChanged = true;
		} //if
		if (server.hasArg("maxOvershot")) {
			maxOvershot = server.arg("maxOvershot").toFloat();
			CONSOLE("SET maxOvershot: ");
			CONSOLELN(maxOvershot);
			configChanged = true;
		} //if
		if (server.hasArg("maxUndershot")) {
			maxUndershot = server.arg("maxUndershot").toFloat();
			CONSOLE("SET maxUndershot: ");
			CONSOLELN(maxUndershot);
			configChanged = true;
		} //if
		if (server.hasArg("idleTime")) {
			idleTime = server.arg("idleTime").toInt();
			CONSOLE("SET idleTime: ");
			CONSOLELN(idleTime);
			configChanged = true;
		} //if
		if (server.hasArg("measuringTime")) {
			measuringTime = server.arg("measuringTime").toInt();
			CONSOLE("SET measuringTime: ");
			CONSOLELN(measuringTime);
			configChanged = true;
		} //if
		if (server.hasArg("ubiToken")) {
			debugToUbidots = server.hasArg("debugToUbidots");
			CONSOLE("SET debugToUbidots: ");
			CONSOLELN(debugToUbidots);
			configChanged = true;
		} //block

		StaticJsonBuffer<300> jsonBuffer;
		JsonObject &json = jsonBuffer.createObject();
		json["targetTemp"] = targetTemp;
		json["hysterese"] = hysterese;
		json["maxOvershot"] = maxOvershot;
		json["maxUndershot"] = maxUndershot;
		json["idleTime"] = idleTime;
		json["measuringTime"] = measuringTime;
		json["ubiToken"] = ubiToken;
		json["debugToUbidots"] = debugToUbidots;

		char jsonchar[300];
		json.printTo(jsonchar);
		server.send(200, "application/json", jsonchar);

		delay(100);
	}); //server.on configApi

	server.on("/apModeApi", []() {
		CONSOLELN("server.on /apModeApi");
		if (server.hasArg("deviceName")) {
			strcpy(deviceName, server.arg("deviceName").c_str());
			CONSOLE("SET deviceName: ");
			CONSOLELN(deviceName);
			configChanged = true;
			if ((String)deviceName != persWM.getApSsid())
			{
				CONSOLE("Servername changed from '");
				CONSOLE(persWM.getApSsid());
				CONSOLE("' to '");
				CONSOLE(deviceName);
				CONSOLELN("'");
				configApModeChanged = true;
			}
		} //if
		if (server.hasArg("apModePsk")) {
			strcpy(apModePsk, server.arg("apModePsk").c_str());
			CONSOLE("SET apModePsk: ");
			CONSOLELN(apModePsk);
			configApModeChanged = true;
		} //if
		StaticJsonBuffer<200> jsonBuffer;
		JsonObject &json = jsonBuffer.createObject();
		json["deviceName"] = deviceName;
		json["apModePsk"] = apModePsk;

		char jsonchar[200];
		json.printTo(jsonchar); //print to char array, takes more memory but sends in one piece
		server.send(200, "application/json", jsonchar);
	}); //server.on Reboot
	server.on("/reboot", []() {
		CONSOLELN("server.on /reboot");
		saveConfig();
		server.sendHeader("Location", String("/index.htm"), true);
		server.send(302, "text/plain", "");
		delay(100);
		ESP.restart();
	}); //server.on apModeApi



	server.begin();
}

bool isDS18B20_Error()
{
	if (currentTemp == DEVICE_DISCONNECTED_C || currentTemp == 85)
	{
		return true;
	}
	return false;
}

void setRelais()
{
	CONSOLELN("SET RELAIS");

	switch (operationMode)
	{
	case idle:
		digitalWrite(MODE_RELAIS, HIGH);
		digitalWrite(PELTIER_RELAIS, HIGH);
		digitalWrite(PUMP_RELAIS, HIGH);
		break;
	case measuring:
		digitalWrite(MODE_RELAIS, HIGH);
		digitalWrite(PELTIER_RELAIS, HIGH);
		digitalWrite(PUMP_RELAIS, LOW);
		break;
	case cooling:
		digitalWrite(MODE_RELAIS, HIGH);
		digitalWrite(PELTIER_RELAIS, LOW);
		digitalWrite(PUMP_RELAIS, LOW);
		break;
	case heating:
		digitalWrite(MODE_RELAIS, LOW);
		digitalWrite(PELTIER_RELAIS, LOW);
		digitalWrite(PUMP_RELAIS, LOW);
		break;
	case dsError:
		digitalWrite(MODE_RELAIS, HIGH);
		digitalWrite(PELTIER_RELAIS, HIGH);
		digitalWrite(PUMP_RELAIS, HIGH);
		break;
	case initializing:
		digitalWrite(MODE_RELAIS, HIGH);
		digitalWrite(PELTIER_RELAIS, HIGH);
		digitalWrite(PUMP_RELAIS, HIGH);
		break;
	default:
		digitalWrite(MODE_RELAIS, HIGH);
		digitalWrite(PELTIER_RELAIS, HIGH);
		digitalWrite(PUMP_RELAIS, HIGH);
		break;
	}
}

bool readConfig()
{
	CONSOLE(F("mounting FS..."));

	if (SPIFFS.begin())
	{
		CONSOLELN(F(" mounted!"));
		if (SPIFFS.exists(CFGFILE))
		{
			CONSOLELN(F("reading config file"));
			File configFile = SPIFFS.open(CFGFILE, "r");
			if (configFile)
			{
				CONSOLELN(F("opened config file"));
				size_t size = configFile.size();
				std::unique_ptr<char[]> buf(new char[size]);

				configFile.readBytes(buf.get(), size);
				DynamicJsonBuffer jsonBuffer;
				JsonObject &json = jsonBuffer.parseObject(buf.get());

				if (json.success())
				{
					CONSOLELN(F("\nparsed json"));

					if (json.containsKey("deviceName"))
						strcpy(deviceName, json["deviceName"]);
					if (json.containsKey("token"))
						strcpy(ubiToken, json["token"]);
					if (json.containsKey("apModePSK"))
						strcpy(apModePsk, json["apModePsk"]);
					if (json.containsKey("targetTemp"))
						targetTemp = (const float)json["targetTemp"];
					if (json.containsKey("hysterese"))
						hysterese = (const float)json["hysterese"];
					if (json.containsKey("measuringTime"))
						measuringTime = (const uint)json["measuringTime"];
					if (json.containsKey("maxOvershot"))
						maxOvershot = (const float)json["maxOvershot"];
					if (json.containsKey("maxUndershot"))
						maxUndershot = (const float)json["maxUndershot"];
					if (json.containsKey("coolingRate"))
						coolingRate = (const float)json["coolingRate"];
					if (json.containsKey("heatingRate"))
						heatingRate = (const float)json["heatingRate"];
					if (json.containsKey("debugToUbidots"))
						debugToUbidots = (bool)json["debugToUbidots"];

					settingsChanged();

					CONSOLELN(F("parsed config:"));
					return true;
				}
				else
				{
					CONSOLELN(F("ERROR: failed to load json config"));
					return false;
				}
			}
			CONSOLELN(F("ERROR: unable to open config file"));
			return false;
		}
	}
	else
	{
		CONSOLELN(F(" ERROR: failed to mount FS!"));
		return false;
	}
	return false;
}

bool saveConfig()
{
	if (!configChanged)
		return true;

	configChanged = false;

	CONSOLE(F("saving config..."));

	if (!SPIFFS.begin())
	{
		CONSOLELN("error starting SPIFFS");
		return false;
	}

	if (!SPIFFS.open(CFGFILE, "w"))
	{
		CONSOLELN("error opening config file (write mode)");
		return false;
	}

	DynamicJsonBuffer jsonBuffer;
	JsonObject &json = jsonBuffer.createObject();

	json["deviceName"] = deviceName;
	json["token"] = ubiToken;
	json["apModePsk"] = apModePsk;
	json["targetTemp"] = targetTemp;
	json["hysterese"] = hysterese;
	json["measuringTime"] = measuringTime;
	json["maxOvershot"] = maxOvershot;
	json["maxUndershot"] = maxUndershot;
	json["coolingRate"] = coolingRate;
	json["heatingRate"] = heatingRate;
	json["debugToUbidots"] = debugToUbidots;

	File configFile = SPIFFS.open(CFGFILE, "w+");
	if (!configFile)
	{
		CONSOLELN(F("failed to open config file for writing"));
		SPIFFS.end();
		return false;
	}
	else
	{
		json.printTo(configFile);
		configFile.close();
		SPIFFS.end();
		CONSOLELN(F("saved successfully"));
		return true;
	}
}

void settingsChanged()
{
	range.set(targetTemp, hysterese, maxUndershot, maxOvershot);
	ambientImpact = 0;
	ambientImpactRelatedTo = airNothing;
	workingTime = WORKING_TIME;
	idleTime = IDLE_TIME;
}

void resetTimers()
{
	idleMetro.interval(idleTime * 1000);
	measuringMetro.interval(measuringTime * 1000);
	workingMetro.interval(workingTime * 1000);

	idleMetro.reset();
	measuringMetro.reset();
	workingMetro.reset();
	oneMinuteMetro.reset();
}

void setup()
{

	CONSOLELN("Setup starting ...");

	SPIFFS.begin();
	readConfig();

	settingsChanged();

	initOled();

	oled.setFont(u8x8_font_pxplusibmcgathin_f);

	oled.draw2x2String(0, 0, "Setup");

	pinMode(ONE_WIRE_BUS, OUTPUT);
	pinMode(MODE_RELAIS, OUTPUT_OPEN_DRAIN);
	pinMode(PELTIER_RELAIS, OUTPUT_OPEN_DRAIN);
	pinMode(PUMP_RELAIS, OUTPUT_OPEN_DRAIN);
	pinMode(LED_BUILTIN, OUTPUT);

	initDS18B20();
	setRelais();

	initConfigManager();

	oled.draw2x2String(0, 3, "done ...");
	CONSOLELN("Setup done");
}

bool uploadData()
{
	SenderClass sender;
	sender.add("currentTemp", currentTemp);
	sender.add("measuredTemp", measuredTemp != 0 ? measuredTemp : currentTemp);
	sender.add("targetTemp", targetTemp);
	if (debugToUbidots)
	{
		sender.add("mode", (int)operationMode);
		sender.add("coolingRate", coolingRate);
		sender.add("heatingRate", heatingRate);
		sender.add("currentTempAmbient", range.getCurrentTempRelatedToAmbientImpact(ambientImpactRelatedTo));
		sender.add("workingTime", workingTime / 60);
		sender.add("tempRangeLow", range.getRangeLowerTemp());
		sender.add("tempRangeHigh", range.getRangeUpperTemp());
	}
	CONSOLELN(F("\ncalling Ubidots"));
	return sender.sendUbidots(ubiToken, deviceName);
}

void updateAmbientImpact()
{
	float tempEndIdle = measuredTemp;
	ambientImpact = (tempEndIdle - tempStartIdle) / (idleTime / 60);
	if (isnan(ambientImpact) || isinf(ambientImpact))
		ambientImpact = 0;
	ambientImpact = ambientImpact == 0 ? 0 :
		(ambientImpact > 0 ? hysterese / 2 : -1 * hysterese / 2);
	range.updateAmbientImpact(ambientImpact);
	CONSOLE("AMBIENT IMPACT  (°C): ");
	CONSOLELN(ambientImpact);
}

void updateCoolingRate()
{
	float tempEndCooling = measuredTemp;
	float effectiveWorkingTime = (workingTime / 60) - 0.75f;
	if (effectiveWorkingTime <= 0)
		return;
	coolingRate = (tempStartCooling - tempEndCooling) / effectiveWorkingTime;
	if (isnan(coolingRate) || isinf(coolingRate))
		coolingRate = 0;
	CONSOLE("COOLING RATE (°C/min): ");
	CONSOLELN(coolingRate);
}

void updateHeatingRate()
{
	float tempEndHeating = measuredTemp;
	float effectiveWorkingTime = (workingTime / 60) - 0.75f;
	if (effectiveWorkingTime <= 0)
		return;
	heatingRate = (tempEndHeating - tempStartHeating) / effectiveWorkingTime;
	if (isnan(heatingRate) || isinf(heatingRate))
		heatingRate = 0;
	CONSOLE("HEATING RATE (°C/min): ");
	CONSOLELN(heatingRate);
}

void updateTemperature(float temp)
{
	currentTemp = temp;
	range.updateCurrentTemp(temp);

	CONSOLE("TEMP: ");
	CONSOLELN(temp);
}

bool getTemperature(bool silent)
{
	float t = currentTemp;
	DS18B20.requestTemperatures();

	t = DS18B20.getTempCByIndex(0);

	if (isDS18B20_Error())
	{
		CONSOLELN(F("ERROR: DS18B20 DISCONNECTED"));
		digitalWrite(ONE_WIRE_BUS, LOW);
		delay(100);
		oneWire.reset();
	}
	updateTemperature(t);

	return !isDS18B20_Error();
}

void startIdle()
{
	idleTime = range.getAssumedTimeToIdle(ambientImpact);
	if (idleTime < MIN_IDLE_TIME)
		idleTime = MIN_IDLE_TIME;
	else if (idleTime > MAX_IDLE_TIME)
		idleTime = MAX_IDLE_TIME;

	CONSOLE("IDLE TIME (s): ");
	CONSOLELN(idleTime);
	resetTimers();
	operationMode = idle;

	tempStartIdle = currentTemp;
}

void startMeasuring()
{
	resetTimers();
	operationMode = measuring;
}

void startCooling()
{
	workingTime = range.getAssumedTimeToChill(coolingRate);
	if (workingTime < MIN_WORKING_TIME)
		workingTime = MIN_WORKING_TIME;
	else if (workingTime > MAX_WORKING_TIME)
		workingTime = MAX_WORKING_TIME;

	CONSOLE("COOLING TIME (s): ");
	CONSOLELN(workingTime);

	resetTimers();
	operationMode = cooling;

	tempStartCooling = currentTemp;
}

void startHeating()
{
	workingTime = range.getAssumedTimeToHeat(heatingRate);
	if (workingTime < MIN_WORKING_TIME)
		workingTime = MIN_WORKING_TIME;
	else if (workingTime > MAX_WORKING_TIME)
		workingTime = MAX_WORKING_TIME;

	CONSOLE("HEATING TIME (s): ");
	CONSOLELN(workingTime);

	resetTimers();
	operationMode = heating;

	tempStartHeating = currentTemp;
}

OperationMode getOperationMode()
{
	if (isDS18B20_Error())
	{
		resetTimers();
		operationMode = dsError;
		return operationMode;
	}

	switch (operationMode)
	{
	case idle:
		if (idleMetro.check())
		{
			lastWorkingMode = idle;
			startMeasuring();
		}
		break;
	case measuring:
		if (measuringMetro.check())
		{
			measuredTemp = currentTemp;

			if (lastWorkingMode == idle)
				updateAmbientImpact();
			else if (lastWorkingMode == cooling)
				updateCoolingRate();
			else if (lastWorkingMode == heating)
				updateHeatingRate();

			if (range.isInBounds(ambientImpactRelatedTo))
			{
				startIdle();
			}
			else if (range.isBelow(btRangeLowerTemp, ambientImpactRelatedTo))
			{
				if (lastWorkingMode == idle)
					ambientImpactRelatedTo = airWillHeat;
				startHeating();
			}
			else if (range.isAbove(btRangeUpperTemp, ambientImpactRelatedTo))
			{
				if (lastWorkingMode == idle)
					ambientImpactRelatedTo = airWillChill;
				startCooling();
			}
			else
			{
				startIdle();
			}
		}
		break;
	case cooling:
		if (workingMetro.check() || currentTemp < targetTemp - maxUndershot)
		{
			lastWorkingMode = cooling;
			startMeasuring();
		}
		if (oneMinuteMetro.check() && currentTemp > targetTemp)
		{
			measuredTemp = currentTemp;
		}
		break;
	case heating:
		if (workingMetro.check() || currentTemp > targetTemp + maxOvershot)
		{
			lastWorkingMode = heating;
			startMeasuring();
		}
		if (oneMinuteMetro.check() && currentTemp < targetTemp)
		{
			measuredTemp = currentTemp;
		}
		break;
	case initializing:
		startMeasuring();
		break;
	case dsError:
		if (!isDS18B20_Error())
			startMeasuring();
		break;
	default:
		break;
	}
	return operationMode;
}

void blink(OperationMode mode)
{
	unsigned long lowTime;
	unsigned long highTime;

	switch (mode)
	{
	case idle:
		lowTime = 15000;
		highTime = 250;
		break;
	case measuring:
		lowTime = 10000;
		highTime = 10000;
		break;
	case cooling:
		lowTime = 3000;
		highTime = 250;
		break;
	case heating:
		lowTime = 250;
		highTime = 3000;
		break;
	case initializing:
		lowTime = 3000;
		highTime = 3000;
		break;
	case dsError:
		lowTime = 250;
		highTime = 250;
		break;
	default:
		lowTime = 100;
		highTime = 100;
		break;
	}

	if (ledMetro.check() == 1) {
		if (ledState == HIGH) {
			ledState = LOW;
			ledMetro.interval(lowTime);
		}
		else {
			ledMetro.interval(highTime);
			ledState = HIGH;
		}
		digitalWrite(LED_BUILTIN, ledState);
	}
}

void displayClearValueLines()
{
	oled.clearLine(3);
	oled.clearLine(4);
	oled.clearLine(6);
	oled.clearLine(7);
};

const char* getDisplayTemp(OperationMode currentMode)
{
	float displayTemp;

	switch (currentMode)
	{
	case idle:
		copyTileArray(Equal_TileUpperLine, TempType_TileUpperLine);
		copyTileArray(Equal_TileBottomLine, TempType_TileBottomLine);
		displayTemp = measuredTemp;
		break;
	case initializing:
		copyTileArray(DummyTileLine , TempType_TileUpperLine);
		copyTileArray(DummyTileLine, TempType_TileBottomLine);
		displayTemp = 0;
		break;
	case dsError:
		copyTileArray(DummyTileLine, TempType_TileUpperLine);
		copyTileArray(DummyTileLine, TempType_TileBottomLine);
		displayTemp = 0;
		break;
	default:
		copyTileArray(Approx_TileUpperLine, TempType_TileUpperLine);
		copyTileArray(Approx_TileBottomLine, TempType_TileBottomLine);
		displayTemp = currentTemp;
		break;
	}

	return floatToString(displayTemp, 1);

}

void loop()
{
	OperationMode previousMode = operationMode;

	persWM.handleWiFi();
	dnsServer.processNextRequest();
	server.handleClient();

	bool getTempResult = false;

	if (getTempMetro.check())
	{
		bool getTempResult;
		getTempResult = getTemperature(false) && !isDS18B20_Error();
		if (!getTempResult)
		{
			getTempResult = getTemperature(true) && !isDS18B20_Error();
		}
		getTempMetro.interval(DS_TIME * 1000);
	}

	bool modeChanged = getOperationMode() != previousMode;


	if (modeChanged)
		setRelais();

	if (getTempResult || configChanged)
	{
		CONSOLE("temp: ");
		CONSOLE(currentTemp);
		CONSOLE(" / target: ");
		CONSOLE(targetTemp);
		CONSOLE(" / opMode: ");
		CONSOLELN(operationModeToString());
	}
	const char* displayTempS = getDisplayTemp(operationMode);
	bool displayTempChanged = strcmp(displayTempS, lastDisplayedTempS) != 0;

	if (modeChanged || configChanged || displayTempChanged)
	{
		strcpy(lastDisplayedTempS, displayTempS);

		if (modeChanged || configChanged)
		{
			CONSOLE("Mode changed: ");
			CONSOLE(operationModeToString());
			CONSOLELN(" ...");
			oled.clearDisplay();
			oled.draw2x2UTF8(0, 0, operationModeToString());
			oled.drawTile(0, 2, 16, Line_Tile);
		}

		if (operationMode == dsError)
		{
			displayClearValueLines();
			oled.draw2x2UTF8(0, 3, "DS18B20");
			oled.draw2x2String(0, 6, "Fehler!");
		}
		else
		{
			displayClearValueLines();
			oled.drawTile(0, 3, 2, TempType_TileUpperLine);
			oled.drawTile(0, 4, 2, TempType_TileBottomLine);
			oled.draw2x2UTF8(2, 3, displayTempS);
			oled.drawTile(11, 3, 2, GradC_TileUpperLine);
			oled.drawTile(11, 4, 2, GradC_TileBottomLine);
			oled.drawTile(0, 6, 2, Arrow_TileUpperLine);
			oled.drawTile(0, 7, 2, Arrow_TileBottomLine);
			oled.draw2x2String(2, 6, floatToString(targetTemp, 1));
			oled.drawTile(11, 6, 2, GradC_TileUpperLine);
			oled.drawTile(11, 7, 2, GradC_TileBottomLine);
		}
	}

	bool sendDataNow = (sendDataMetro.check() || modeChanged)
		&& ubiToken != (char*)""
		&& (WiFi.getMode() == WIFI_STA || WiFi.getMode() == WIFI_AP_STA) && WiFi.isConnected()
		&& currentTemp > 0
		&& currentTemp < 85;

	if (sendDataNow)
	{
		sendDataMetro.reset();
		uploadData();
	}

	if (configChanged || configApModeChanged)
	{
		saveConfig();
		settingsChanged();
	}

	if (configApModeChanged)
	{
		configApModeChanged = false;
		ESP.restart();
	}

	blink(operationMode);
}
